//
//  Team.swift
//  IBMFC
//
//  Created by Wendell Tufano on 26/12/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import Foundation

class Team {
    let teamId:Int
    let name:String
    var victory:Int = 0
    var lose:Int = 0
    var draw:Int = 0
    var goals:Int = 0
    var points:Int = 0
    
    init(teamId:Int, name:String){
        self.teamId = teamId
        self.name =  name
    }
}
