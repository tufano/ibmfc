//
//  StrikerJSON.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import Foundation

class StrikersJSON:Codable{
    let status:String
    let data:[Striker]
}

class Striker:Codable {
    let playerId:Int
    let playerName:String
    let goals:Int
    let teamId:Int
    let playerPhotoURL:String
}
