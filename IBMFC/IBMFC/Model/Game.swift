//
//  Game.swift
//  IBMFC
//
//  Created by Wendell Tufano on 26/12/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import Foundation

class Game {
    let gameId:Int
    let teamHomeId:Int
    let teamAwayId:Int
    let scoreHomeId:Int
    let scoreAwayId:Int
    
    init(gameId:Int,teamHomeId:Int,teamAwayId:Int,scoreHomeId:Int,scoreAwayId:Int) {
        self.gameId = gameId
        self.teamHomeId = teamHomeId
        self.teamAwayId = teamAwayId
        self.scoreHomeId = scoreHomeId
        self.scoreAwayId = scoreAwayId
    }
}
