//
//  GameJSON.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import Foundation

struct GameData:Codable{
    let status:String
    let data:[GameJSON]
}

struct GameJSON:Codable {
    let gameId:Int
    let teamHomeId:Int
    let teamAwayId:Int
    let scoreHomeId:Int
    let scoreAwayId:Int
}
