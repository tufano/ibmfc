//
//  TeamJSON.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import Foundation

struct TeamJSON:Codable {
    let teams:[DataTeam]
}

struct DataTeam:Codable {
    let teamId:Int
    let name:String
}

