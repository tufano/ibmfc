//
//  GameService.swift
//  IBMFC
//
//  Created by Wendell Tufano on 26/12/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import Foundation
import Alamofire
import SwiftKeychainWrapper

struct GameRequest {
    
    static func request(onComplete: @escaping ([Game]?)->Void) {
        var gamesJSON:[GameJSON] = []
        var games:[Game] = []
        Alamofire.request("https://private-anon-6f31fd0848-ibmfc.apiary-mock.com/games").responseJSON { (response) in
            guard let data = response.data,
                let gameData = try? JSONDecoder().decode(GameData.self, from: data) else{ return onComplete(nil)}
            
            DispatchQueue.main.async {
                gamesJSON = gameData.data
                for game in gamesJSON {
                    let newGame = Game(gameId: game.gameId , teamHomeId: game.teamHomeId , teamAwayId: game.teamAwayId, scoreHomeId: game.scoreHomeId, scoreAwayId: game.scoreAwayId)
                    games.append(newGame)
                }
                DispatchQueue.main.async {
                    onComplete(games)
                }
            }
        } 
    }
}

struct LoginRequest {
    
    static func request(bodyRequest:Parameters){
        let request = Alamofire.request("https://private-anon-6f31fd0848-ibmfc.apiary-mock.com/login", method:.post, parameters:bodyRequest, encoding: JSONEncoding.default)
        
        if let requestURL = request.request{
            NetworkLog.logRequest(requestURL)
        }
        request.responseJSON { (response) in
            NetworkLog.logResponse(response.response)
            guard let data = response.data,
                let autheticatorResponse = try? JSONDecoder().decode(Info.self, from: data) else { return }
            
                //Return API request
                let acessToken = autheticatorResponse.data.token
                let userAcess = autheticatorResponse.success
            
                //Save acess in keyChain
                KeychainWrapper.standard.set(acessToken, forKey: "acessToken")
                KeychainWrapper.standard.set(userAcess, forKey: "userAccess")

        }
    }
}

struct TeamRequest {
    
    static func request() -> [DataTeam] {
        var teams:[DataTeam] = []
        
        let fileURL = Bundle.main.url(forResource: "Teams.json", withExtension: nil)
        guard let file = fileURL,
            let data = try? Data(contentsOf: file) else {
                return teams
        }
        do{
            let dataTeams = try JSONDecoder().decode(TeamJSON.self, from: data)
            teams.append(contentsOf: dataTeams.teams)
            return teams
        }catch{
            print(error.localizedDescription)
        }
        return teams
    }
}

struct StrikesRequest{
    
    static func request(onComplete: @escaping ([Striker]?)-> Void){
        let request = Alamofire.request("https://private-anon-6f31fd0848-ibmfc.apiary-mock.com/strikers")
        if let requestURL = request.request{
            NetworkLog.logRequest(requestURL)
        }
        request.responseJSON { (response) in
            NetworkLog.logResponse(response.response)
            guard let data = response.data,
                let strikerJSON = try? JSONDecoder().decode(StrikersJSON.self, from: data) else { return }
            DispatchQueue.main.async {
               onComplete(strikerJSON.data)
            }
        }
    }
}
