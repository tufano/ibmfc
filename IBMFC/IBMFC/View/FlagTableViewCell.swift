//
//  FlagTableViewCell.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import UIKit

class FlagTableViewCell: UITableViewCell {

    @IBOutlet weak var flagImage: UIImageView!
    @IBOutlet weak var lbNameFlag: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func prepareFlagCell(with team:DataTeam){
        
        flagImage.image = UIImage(named: "\(team.teamId)-\(team.name)")
        lbNameFlag.text = team.name.replacingOccurrences(of: "_", with: " ")
        
    }
}
