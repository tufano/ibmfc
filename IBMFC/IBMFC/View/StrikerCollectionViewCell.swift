//
//  StrikerCollectionViewCell.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import UIKit
import Kingfisher

class StrikerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageStriker: UIImageView!
    @IBOutlet weak var flagStriker: UIImageView!
    @IBOutlet weak var textScore: UILabel!
    @IBOutlet weak var labelNumber: UILabel!
    
    func prepareStriekrCell(with striker:Striker, flagName:String, positionNumber:Int){
        
        flagStriker.image = UIImage(named: "\(striker.teamId)-\(flagName)")
        textScore.text = "\(striker.goals)GOLS"
        if let imageURL = URL(string: striker.playerPhotoURL){
            imageStriker.kf.indicatorType = .activity
            imageStriker.kf.setImage(with: imageURL)
        }
        labelNumber.text = "\(positionNumber)"
    }
    
}
