//
//  TeamTableViewCell.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import UIKit

class TeamTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgFlag: UIImageView!
    @IBOutlet weak var labelNameTeam: UILabel!
    
    @IBOutlet weak var buttonPoints: UIButton!
    @IBOutlet weak var buttonVictory: UIButton!
    @IBOutlet weak var buttonDraw: UIButton!
    @IBOutlet weak var buttonLose: UIButton!
    @IBOutlet weak var buttonGoals: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func prepareTeamCell(with team:Team){
        imgFlag.image = UIImage(named: "\(team.teamId)-\(team.name)")
        labelNameTeam.text = team.name.replacingOccurrences(of: "_", with: " ")
        buttonPoints.setTitle("\(team.points)", for: .normal)
        buttonVictory.setTitle("\(team.victory)", for: .normal)
        buttonDraw.setTitle("\(team.draw)", for: .normal)
        buttonLose.setTitle("\(team.lose)", for: .normal)
        buttonGoals.setTitle("\(team.goals)", for: .normal)
    }

}
