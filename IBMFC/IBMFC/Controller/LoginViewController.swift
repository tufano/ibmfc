//
//  LoginViewController.swift
//  IBMFC
//
//  Created by Wendell Tufano on 29/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import UIKit
import Alamofire
import SwiftKeychainWrapper

class LoginViewController: UIViewController {
    
    //MARK: - PROPERTIES
    var saveAcessToken:Bool = false
    var saveUserAcess:Bool = false
    var isLoggin:Bool? = false

    //MARK: - COMPONENTS
    @IBOutlet weak var tfLogin: CustomUITextField!
    @IBOutlet weak var tfPassword: CustomUITextField!
    
    //MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - BUTTON ACTION
    @IBAction func loginAuthentication(_ sender: Any) {
        loginAuthenticator()
    }
    
    @IBAction func didUnwindFromHome(_ sender: UIStoryboardSegue){
        let _ = KeychainWrapper.standard.removeAllKeys()
        tfPassword.text = nil
    }
    
    //MARK: - FUNC
    func loginAuthenticator(){
        
        if tfLogin.text!.isEmpty && tfPassword.text!.isEmpty {
            
            // create the alert
            let alert = UIAlertController(title: "Campo(s) Vazio(s)", message: "Favor preencher todos os campos.", preferredStyle: .alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
            return
            
        } else {
            
            guard let user = tfLogin.text else {return}
            guard let pass = tfPassword.text else {return}
            
            //Body Request
            let parameters:Parameters = ["username": "\(user)","password": "\(pass)"]
            
            validateLogin(parameters: parameters)
            
        }
    } 
    
    func validateLogin(parameters: Parameters){
        
        DispatchQueue.main.async {
            LoginRequest.request(bodyRequest: parameters)
        }
        
        if(KeychainWrapper.standard.bool(forKey: "userAccess") ?? false){
            loginDone()
        }
    
    }
    
    func loginDone(){
        //Next Screen
        performSegue(withIdentifier: "indexSegue", sender: nil)
    }
}

//MARK: - EXTENSION VIEWCONTROLLER
extension LoginViewController{
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
}
