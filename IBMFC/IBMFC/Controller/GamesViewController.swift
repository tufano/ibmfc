//
//  GamesViewController.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import UIKit

class GamesViewController: UIViewController {

    //MARK: - PROPERTIES
    var isHome = true
    var homeName:String? = nil
    var awayName:String? = nil
    
    //MARK: - COMPONENTS
    @IBOutlet weak var homeFlag: UIButton!
    @IBOutlet weak var awayFlag: UIButton!
    @IBOutlet weak var lbNameHome: UILabel!
    @IBOutlet weak var lbNameAway: UILabel!
    @IBOutlet weak var textHomeGoal: CustomUITextField!
    @IBOutlet weak var textAwayGoal: CustomUITextField!
    
    //MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - BUTTON ACTION
    @IBAction func showHomeFlag(_ sender: Any) {
        performSegue(withIdentifier: "flagsSegue", sender: nil)
        isHome = true
    }
    
    @IBAction func showAwayFlag(_ sender: Any) {
        performSegue(withIdentifier: "flagsSegue", sender: nil)
        isHome = false
    }
    
    @IBAction func buttonExit(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didUnwindFromFlag(_ sender: UIStoryboardSegue){
        
        guard let flagVC = sender.source as? FlagsViewController else {return}
        
        if isHome{
            
            if flagVC.flagName == nil && flagVC.flagId == nil {
                lbNameHome.text = "País 1"
            } else{
                homeName = flagVC.flagName?.replacingOccurrences(of: "_", with: " ")
                let uppercasedName = homeName?.uppercased()
                
                if lbNameAway.text != uppercasedName {
                    lbNameHome.textColor = UIColor.black
                    lbNameHome.text = uppercasedName
                    let bgButton = UIImage(named: "\(flagVC.flagId!)-\(flagVC.flagName!)")
                    homeFlag.setImage(bgButton, for: .normal)
                }
            }
        } else{
            
            if flagVC.flagName == nil && flagVC.flagId == nil {
                lbNameAway.text = "País 2"
            } else{
                awayName = flagVC.flagName?.replacingOccurrences(of: "_", with: " ")
                let uppercasedName = awayName?.uppercased()
                
                if lbNameHome.text != uppercasedName{
                    lbNameAway.textColor = UIColor.black
                    lbNameAway.text = uppercasedName
                    let bgButton = UIImage(named: "\(flagVC.flagId!)-\(flagVC.flagName!)")
                    awayFlag.setImage(bgButton, for: .normal)
                }
            }
        }
    }
}

