//
//  FlagsViewController.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import UIKit

class FlagsViewController: UIViewController {

    //MARK: - PROPERTIES
    var flags:[DataTeam] = []
    var searchFlag:[DataTeam] = []
    var isSearching = false
    var flagName:String? = nil
    var flagId:Int? = nil
    
    //MARK: - OUTLETS
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var exitButton: UIBarButtonItem!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    //MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        loadTeams()
    }
    
    //MARK: - FUNC
    func loadTeams(){
        flags = TeamRequest.request()
    }
    
    //MARK: - BUTTONS ACTION
    @IBAction func exit(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - TABLEVIEW DELEGATE PROTOCOL
extension FlagsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return searchFlag.count
        }
        return flags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "flagCell", for: indexPath) as! FlagTableViewCell
        if isSearching {
            let team = searchFlag[indexPath.row]
            cell.prepareFlagCell(with: team)
        } else {
            let team = flags[indexPath.row]
            cell.prepareFlagCell(with: team)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //tableVieW.cellForRow(at: indexPath)?.accessoryType = UITableViewCell.AccessoryType.checkmark
        if isSearching {
            self.flagName = searchFlag[indexPath.row].name
            self.flagId = searchFlag[indexPath.row].teamId
        } else {
            self.flagName = flags[indexPath.row].name
            self.flagId = flags[indexPath.row].teamId
        }
    }
}

//MARK: - SEARCHBAR DELEGATE
extension FlagsViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let formatedText = searchText.replacingOccurrences(of: " ", with: "_")
        searchFlag = flags.filter({$0.name.prefix(formatedText.count) == formatedText})
        isSearching = true
        tbView.reloadData()
    }
}

