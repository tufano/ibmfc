//
//  HomeViewController.swift
//  IBMFC
//
//  Created by Wendell Tufano on 30/08/18.
//  Copyright © 2018 Wendell Tufano. All rights reserved.
//

import UIKit
import Alamofire

class HomeViewController: UIViewController {
    
    //MARK: - PROPERTIES
    var teams:[Team] = []
    var strikers:[Striker] = []
    var currentGameId = 21
    
    //MARK: - COMPONENTS
    @IBOutlet weak var resultTableView: UITableView!
    @IBOutlet weak var strikerCollectionView: UICollectionView!

    //MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        loadStaticTeams()
        loadStaticScore()
        loadStrikers()
    }
    
    //MARK: - BUTTON EXIT
    @IBAction func didUnwindFromGames(_ sender: UIStoryboardSegue){
        
        guard let gameVC = sender.source as? GamesViewController else { return }
        let searchHomeTeam = teams.filter({$0.name == gameVC.homeName?.replacingOccurrences(of: " ", with: "_")})
        let searchAwayTeam = teams.filter({$0.name == gameVC.awayName?.replacingOccurrences(of: " ", with: "_")})
        
        if searchHomeTeam.isEmpty || searchAwayTeam.isEmpty{
            return
        }
        
        //Result from GamesViewController
        guard let textHome = gameVC.textHomeGoal.text else { return }
        guard let textAway = gameVC.textAwayGoal.text else { return }
        guard let scoreHome = Int(textHome) else {return}
        guard let scoreAway = Int(textAway) else {return}
        
        //New object game
        let newGame = Game(gameId: currentGameId, teamHomeId: searchHomeTeam[0].teamId , teamAwayId: searchAwayTeam[0].teamId, scoreHomeId: scoreHome, scoreAwayId: scoreAway)
        
        currentGameId += 1
        generateScore(with: newGame)
        loadNewGames()
        
    }
    
    //MARK: - FUNC
    func loadStaticScore(){
        GameRequest.request { (result) in
            if let gameRequest = result{
                self.generateScore(with: gameRequest)
                self.loadNewGames()
            }
        }
    }
    
    func loadNewGames(){
        resultTableView.reloadData()
    }
    
    func loadStrikers(){
        StrikesRequest.request(onComplete: { (result) in
            if let strikers = result {
                self.strikers = strikers
                self.strikerCollectionView.reloadData()
            }
        })
    }
    
    func loadStaticTeams(){
        let staticTeams = TeamRequest.request()
        for team in staticTeams {
            let loadedTeam = Team(teamId: team.teamId, name: team.name)
            teams.append(loadedTeam)
        }
    }
    
    func generateScore(with gameArray:[Game]?){
        
        if let games = gameArray {
            for game in games{
                generateScore(with: game)
            }
        }
        
    }
    
    func generateScore(with game:Game?){
        
        guard let game = game else { return }
        let homeId = game.teamHomeId - 1
        let awayId = game.teamAwayId - 1
            
        //if home Win
        if game.scoreHomeId > game.scoreAwayId {
            teams[homeId].victory += 1
            teams[homeId].goals += game.scoreHomeId
            teams[homeId].points += 3
                
            teams[awayId].lose += 1
            teams[awayId].goals += game.scoreAwayId
        }
        //if away Win
        if game.scoreAwayId > game.scoreHomeId {
            teams[awayId].victory += 1
            teams[awayId].goals += game.scoreAwayId
            teams[awayId].points += 3
                
            teams[homeId].lose += 1
            teams[homeId].goals += game.scoreHomeId
        }
        //if draw
        if game.scoreHomeId == game.scoreAwayId {
            teams[homeId].draw += 1
            teams[homeId].goals += game.scoreHomeId
            teams[homeId].points += 1
                
            teams[awayId].draw += 1
            teams[awayId].goals += game.scoreAwayId
            teams[awayId].points += 1
        }
    }
}

//MARK: - EXTENSION UITABLEVIEW PROTOCOLS
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamCell", for: indexPath) as! TeamTableViewCell
        
        //Sorted Array
        let sortedArray = teams.sorted(by:{if $0.points == $1.points{
                return $0.goals > $1.goals
            }
            return $0.points > $1.points
        })
        
        //Custom Cell
        let team = sortedArray[indexPath.row]
        cell.prepareTeamCell(with: team)
        return cell
    }
    
}

//MARK: - EXTENSION UICOLLECTIONVIEW PROTOCOLS
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return strikers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "strikerCollectionItem", for: indexPath) as! StrikerCollectionViewCell

        //Sorted Array
        let sortedArray = strikers.sorted(by:{$0.goals > $1.goals})

        //Custom Cell
        let striker = sortedArray[indexPath.row]
            let position = sortedArray.firstIndex(where: {$0 === striker})
                let teamName = teams[striker.teamId - 1].name
        
        if let position = position {
            let positionStrike = position + 1
            cell.prepareStriekrCell(with: striker, flagName: teamName, positionNumber: positionStrike)
        }
        return cell
    }
    
}
