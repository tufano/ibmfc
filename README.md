## IBMFC

IBMFC is a simple application, developeed for iOS with the purpose of managing a soccer championship by using REST API and MVC architecture.

---

## Requirements

* iOS 11.0+

---

## Material used

* API DOC: http://docs.ibmfc.apiary.io
* UIGuide: https://ibm.box.com/v/fc-guide

---

## License

The application was developed for the purposes of a closed test at IBM. All rights reserved to IBM.
